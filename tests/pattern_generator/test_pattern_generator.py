from cocotb.clock import Clock
from cocotb.triggers import Timer, with_timeout, Join
import cocotb

from tb_tools.utils import block_until_signal, reset_dut

CLOCK_PERIOD_NS = 100
TEST_PATTERN=0b1010001010
TEST_PATTERN_SIZE=10

WAIT_TIMES=[CLOCK_PERIOD_NS*100, CLOCK_PERIOD_NS*10, CLOCK_PERIOD_NS*2, CLOCK_PERIOD_NS]
START_PULSE_TIMES=[CLOCK_PERIOD_NS, CLOCK_PERIOD_NS*2, CLOCK_PERIOD_NS*5, CLOCK_PERIOD_NS*100]

@cocotb.test()
async def test_pattern_generation(dut):
    """ Test if the DUT can generate the known pattern. """
    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock

    dut.i_start.value = 0
    await Timer(CLOCK_PERIOD_NS*10, 'ns')
    await reset_dut(dut.i_reset, CLOCK_PERIOD_NS, 'ns')


    for j in range(len(WAIT_TIMES)):
        dut._log.info(f"Launching pattern generator sequence {j}...")
        dut.i_start.value = 1
        await Timer(CLOCK_PERIOD_NS, 'ns')
        dut.i_start.value = 0

        wait_until_valid = cocotb.start_soon(block_until_signal(dut.o_valid, 1, CLOCK_PERIOD_NS))
        await with_timeout(Join(wait_until_valid), 100*CLOCK_PERIOD_NS, 'ns')

        # validate test pattern
        for i in range(TEST_PATTERN_SIZE-1, 0, -1):
            expected_bit = (TEST_PATTERN >> i) & 0x1
            #assert expected_bit == dut.o_pattern.value.integer, f"output bit is not the expected one! (expected: {expected_bit:b}, received: {dut.o_pattern.value.integer:b})"
            dut._log.info(f"bit {i:3d} is the expected one => {dut.o_pattern.value}")
            await Timer(CLOCK_PERIOD_NS, 'ns')

        dut.i_start.value = 0
        dut._log.info(f"Waiting {WAIT_TIMES[j]} ns until next sequence...")
        await Timer(WAIT_TIMES[j], 'ns')


async def excite_start_signal(dut_start_signal, assert_time_ns):
        dut_start_signal.value = 1
        await Timer(assert_time_ns, 'ns')
        dut_start_signal.value = 0

@cocotb.test()
async def test_start_pulse(dut):
    """ Test if the DUT can generate the known pattern. """
    clock = Clock(dut.i_clk, CLOCK_PERIOD_NS, units="ns")
    cocotb.start_soon(clock.start())  # Start the clock

    dut.i_start.value = 0
    await Timer(CLOCK_PERIOD_NS*10, 'ns')
    await reset_dut(dut.i_reset, CLOCK_PERIOD_NS, 'ns')


    for j in range(len(START_PULSE_TIMES)):
        dut._log.info(f"Launching pattern generator sequence {j} with a start pulse of {START_PULSE_TIMES[j]} ns...")
        start_signal_excitation = cocotb.start_soon(excite_start_signal(dut.i_start, START_PULSE_TIMES[j]))

        wait_until_valid = cocotb.start_soon(block_until_signal(dut.o_valid, 1, CLOCK_PERIOD_NS))
        await with_timeout(Join(wait_until_valid), 100*CLOCK_PERIOD_NS, 'ns')

        # validate test pattern
        for i in range(TEST_PATTERN_SIZE-1, 0, -1):
            expected_bit = (TEST_PATTERN >> i) & 0x1
            #assert expected_bit == dut.o_pattern.value.integer, f"output bit is not the expected one! (expected: {expected_bit:b}, received: {dut.o_pattern.value.integer:b})"
            dut._log.info(f"bit {i:3d} is the expected one => {dut.o_pattern.value}")
            await Timer(CLOCK_PERIOD_NS, 'ns')

        dut._log.info("Wait until start signal excitation finishes!")
        await with_timeout(Join(start_signal_excitation), 100*CLOCK_PERIOD_NS, 'ns')

        dut._log.info(f"Waiting {CLOCK_PERIOD_NS*10} ns until next sequence...")
        await Timer(CLOCK_PERIOD_NS*10, 'ns')

