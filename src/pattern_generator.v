/*
 * Verilog code of a pattern generator used to generate patterns
 * to validate funcionality of Crono TDC.
 * Caveats of this pattern generator: the first bit generated will be
 * a dummy (a '1b0'), because the fsm takes 1 clock cycle to start
 * generating the pattern
 *
 * Author: Julian Rodriguez <jnrodriguez@estudiantes.unsam.edu.ar>
 */

module pattern_generator(i_start, i_clk, i_reset, o_pattern, o_valid);
    parameter PATTERN_SIZE = 10;
    parameter [PATTERN_SIZE-1:0] PATTERN = 10'b1010001010;

    input i_start;  // signal that starts the pattern generation
    input i_clk;    // clock
    input i_reset;  // synchronous reset input
    output reg o_pattern; // port where the pattern will be generated
    output o_valid; // port used to notify that the generator is working

    /*- Pattern register ----------------------------------------------------*/
    reg [PATTERN_SIZE-1:0] r_pattern = PATTERN;
    
    /*- Rising edge detector in start signal --------------------------------*/
    wire [1:0] w_start;
    wire w_start_red;
    ffd ffd_red0  ( .D(i_start), .clk(i_clk), .sync_reset(i_reset),  .Q(w_start[0])  );
    ffd ffd_red1  ( .D(w_start[0]), .clk(i_clk), .sync_reset(i_reset),  .Q(w_start[1])  );
    assign w_start_red = w_start[0] & (~w_start[1]);

    /*- Counter that index the pattern register -----------------------------*/
    wire w_counter_reset, w_counter_finished;
    reg [$clog2(PATTERN_SIZE):0] r_counter;

    assign w_counter_reset = i_reset | (w_start_red);
    assign w_counter_finished = (r_counter > PATTERN_SIZE-1) ? 1 : 0;

    always @(posedge i_clk) begin
        if( w_counter_reset == 1 ) begin
            r_counter <= 0;
        end else if( w_counter_finished == 0 ) begin
            r_counter <= r_counter + 1;
        end
    end

    wire w_clear_output = w_counter_finished | w_counter_reset; 

    always @(r_counter, w_clear_output) begin
        if( w_clear_output == 1 ) begin
            o_pattern <= 0;
        end else begin
            o_pattern <= (r_pattern >> r_counter) & 1'b1;
        end
    end

    /*- Flag used to notify when the module is generating the pattern -------*/
    wire w_counter_finished_delayed;
    ffd ffd_counter_finished  ( .D(w_counter_finished), .clk(i_clk), .sync_reset(i_reset),  .Q(w_counter_finished_delayed)  );
    /* Here we catch some signals from where we can infer that the generator have valid data at the output.
     We also use w_start_red here because if not, when w_start_red is high, the condition with r_counter and w_counter_finished_delayed 
     is true and at that point the output of the generator is not valid. 
     */
    assign o_valid = ( (r_counter > 0) && (w_counter_finished_delayed == 0) && (w_start_red == 0)) ? 1 : 0;

// the "macro" to dump signals
`ifdef COCOTB_SIM
initial begin
  $dumpfile ("pattern_generator.vcd");
  $dumpvars (0, pattern_generator);
  #1;
end
`endif

endmodule